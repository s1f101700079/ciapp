package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException {
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "password");
    }

    @Test

    public void testLoginSuccess() throws InvalidPasswordException ,UserNotFound {
            User user = loginManager.login("Testuser1", "password");
            assertThat(user.getUsername(), is("Testuser1"));
            assertThat(user.getPassword(), is("password"));
        }

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws UserNotFound, InvalidPasswordException {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = UserNotFound.class)
        public void testLoginUnregisteredUser() throws InvalidPasswordException,UserNotFound{
                User user = loginManager.login("Iniad", "password");
            }



        }