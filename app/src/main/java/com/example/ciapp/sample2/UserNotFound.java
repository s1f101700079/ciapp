package com.example.ciapp.sample2;

public class UserNotFound extends Exception {
    public UserNotFound(String msg){
        super(msg);
    }

}
